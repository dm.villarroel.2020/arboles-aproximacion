#!/usr/bin/env pythoh3

import sys

def compute_trees(trees, base_trees, fruit_per_tree, reduction):
    if base_trees is None or reduction is None:
        return None

    total_reduction = (trees - base_trees) * reduction
    reduced_fruit = fruit_per_tree - total_reduction
    production = trees * reduced_fruit
    return production

def compute_all(min_trees, max_trees, base_tree, fruit_per_tree, reduction):

    productions = []
    for trees in range(min_trees, max_trees + 1):
        production = compute_trees(trees, base_tree, fruit_per_tree, reduction)
        productions.append((trees, production))
        if production is None:
            return None
        productions.append((trees,production))
    return productions

def read_arguments():

    if len(sys.argv) != 6:
        sys.exit('Usage: aprox.py <base_trees> <fruit_per_tree> <reduction> <min> <max>')
    try:
        base_trees = int(sys.argv[1])
        fruit_per_tree = int(sys.argv[2])
        reduction = int(sys.argv[3])
        min_trees = int(sys.argv[4])
        max_trees = int(sys.argv[5])
    except ValueError:
        sys.exit('Error, inputs must be integers')
    return base_trees, fruit_per_tree, reduction, min_trees, max_trees


def main():

    base_trees, fruit_per_tree, reduction, min_trees, max_trees = read_arguments()
    productions = compute_all(min_trees, max_trees, base_trees, fruit_per_tree, reduction)

    if productions is None:
        sys.exit('Error, invalid inputs')

    best_production = max(productions, key=lambda x: x[1])[1]
    best_trees = max(productions, key=lambda x: x[1])[0]
    for trees, production in productions:
        print(f'{trees} {production}')
    print(f"Best production: {best_production}, for {best_trees} trees")

if __name__ == '__main__':
    main()
